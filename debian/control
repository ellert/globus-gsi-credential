Source: globus-gsi-credential
Priority: optional
Maintainer: Mattias Ellert <mattias.ellert@physics.uu.se>
Build-Depends:
 debhelper-compat (= 13),
 dpkg-dev (>= 1.22.5),
 pkgconf,
 libglobus-common-dev (>= 15),
 libglobus-gsi-openssl-error-dev (>= 2),
 libglobus-gsi-cert-utils-dev (>= 8),
 libglobus-gsi-sysconfig-dev (>= 5),
 libglobus-gsi-callback-dev (>= 4),
 libssl-dev
Build-Depends-Indep:
 doxygen
Standards-Version: 4.7.1
Section: net
Vcs-Browser: https://salsa.debian.org/ellert/globus-gsi-credential
Vcs-Git: https://salsa.debian.org/ellert/globus-gsi-credential.git
Homepage: https://github.com/gridcf/gct/

Package: libglobus-gsi-credential1t64
Provides: ${t64:Provides}
Replaces: libglobus-gsi-credential1
Breaks: libglobus-gsi-credential1 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Description: Grid Community Toolkit - Globus GSI Credential Library
 The Grid Community Toolkit (GCT) is an open source software toolkit used for
 building grid systems and applications. It is a fork of the Globus Toolkit
 originally created by the Globus Alliance. It is supported by the Grid
 Community Forum (GridCF) that provides community-based support for core
 software packages in grid computing.
 .
 The libglobus-gsi-credential1t64 package contains:
 Globus GSI Credential Library

Package: libglobus-gsi-credential-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libglobus-gsi-credential1t64 (= ${binary:Version}),
 ${misc:Depends},
 libglobus-common-dev (>= 15),
 libglobus-gsi-openssl-error-dev (>= 2),
 libglobus-gsi-cert-utils-dev (>= 8),
 libglobus-gsi-sysconfig-dev (>= 5),
 libglobus-gsi-callback-dev (>= 4),
 libssl-dev
Suggests:
 libglobus-gsi-credential-doc (= ${source:Version})
Description: Grid Community Toolkit - Globus GSI Credential Library Development Files
 The Grid Community Toolkit (GCT) is an open source software toolkit used for
 building grid systems and applications. It is a fork of the Globus Toolkit
 originally created by the Globus Alliance. It is supported by the Grid
 Community Forum (GridCF) that provides community-based support for core
 software packages in grid computing.
 .
 The libglobus-gsi-credential-dev package contains:
 Globus GSI Credential Library Development Files

Package: libglobus-gsi-credential-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends}
Description: Grid Community Toolkit - Globus GSI Credential Library Documentation Files
 The Grid Community Toolkit (GCT) is an open source software toolkit used for
 building grid systems and applications. It is a fork of the Globus Toolkit
 originally created by the Globus Alliance. It is supported by the Grid
 Community Forum (GridCF) that provides community-based support for core
 software packages in grid computing.
 .
 The libglobus-gsi-credential-doc package contains:
 Globus GSI Credential Library Documentation Files
